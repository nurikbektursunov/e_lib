from django.contrib.auth import login
from django.db import transaction
from drf_spectacular.utils import extend_schema
from rest_framework import status, generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from rest_framework_simplejwt.tokens import RefreshToken

from .models import User
from users.serializers import (
    OTPSerializer,
    PhoneNumberSerializer,
    UserSerializer,
    UserTokenSerializer
)
from users.utils import generate_otp, get_totp, send_otp_via_sms


class AuthAPIViewMixin:
    permission_classes = [AllowAny]

    def generate_response(self, user, response_status=status.HTTP_200_OK):
        refresh = RefreshToken.for_user(user)
        login(self.request, user)
        return Response(
            {
                "refresh": str(refresh),
                "access": str(refresh.access_token),
            },
            status=response_status
        )


class RegistrationView(generics.CreateAPIView):
    serializer_class = UserSerializer

    @extend_schema(responses={200: UserTokenSerializer})
    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        otp_instance = generate_otp(user)
        try:
            response = send_otp_via_sms(user.phone_number.raw_input, otp_instance)
            return Response(response, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GenerateOTPView(APIView, AuthAPIViewMixin):

    @extend_schema(request=PhoneNumberSerializer)
    def post(self, request):
        serializer = PhoneNumberSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone_number = serializer.validated_data.get('phone_number')
        user = get_object_or_404(User, phone_number=phone_number)
        otp_instance = generate_otp(user)
        response = send_otp_via_sms(phone_number, otp_instance)
        return Response(response, status=status.HTTP_200_OK)


class VerifyOTPView(APIView, AuthAPIViewMixin):

    @extend_schema(request=OTPSerializer, responses={200: UserTokenSerializer})
    @transaction.atomic
    def post(self, request):
        serializer = OTPSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone_number = serializer.validated_data.get('phone_number')
        otp_code = serializer.validated_data.get('otp_code')

        user = get_object_or_404(User, phone_number=phone_number)
        totp = get_totp()

        if totp.verify(otp_code) and not user.otp_is_used:
            user.otp_is_used = True
            user.save()
            return self.generate_response(user)
        else:
            return Response({'error': 'Неверный Код или Код истек'}, status=status.HTTP_400_BAD_REQUEST)


class UserTypeView(APIView):
    def get(self, request):
        return Response({"user_type": User.TypeChoices.values})
