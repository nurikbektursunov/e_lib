from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models

from phonenumber_field.modelfields import PhoneNumberField

from users.managers import UserManager


class Region(models.Model):
    name = models.CharField(max_length=255)


class EduInstitution(models.Model):
    name = models.CharField(max_length=255)
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True, blank=True)


class User(AbstractBaseUser, PermissionsMixin):
    created_at = models.DateTimeField(auto_now_add=True)

    class TypeChoices(models.TextChoices):
        SCHOOL = "Школьник"
        STUDENT = "Студент"
        PARENT = "Родитель"
        OTHER = "Другое"

    phone_number = PhoneNumberField(unique=True)
    full_name = models.CharField(max_length=255, null=True, blank=True)
    birth_data = models.DateField(null=True, blank=True)
    user_type = models.CharField(max_length=10, choices=TypeChoices.choices, default=TypeChoices.OTHER)
    edu_institution = models.ForeignKey(EduInstitution, on_delete=models.SET_NULL, null=True, blank=True)

    is_staff = models.BooleanField(verbose_name="Админ", default=False)

    otp_code = models.CharField(max_length=6, blank=True, null=True)
    otp_is_used = models.BooleanField(default=False)

    USERNAME_FIELD = "phone_number"
    objects = UserManager()

    def __str__(self):
        return f'{self.phone_number}'
