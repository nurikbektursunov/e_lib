from django.urls import path

from users.views import GenerateOTPView, VerifyOTPView, RegistrationView, UserTypeView

urlpatterns = [
    path('register/', RegistrationView.as_view(), name='register'),
    path('otp/generate', GenerateOTPView.as_view(), name='otp_generate'),
    path('otp/verify/', VerifyOTPView.as_view(), name='otp_verify'),
    path('user-type/', UserTypeView.as_view(), name='user_type'),
]
