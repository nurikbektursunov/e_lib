import re

from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers


def validate_phone_number(value: PhoneNumberField):
    pattern = r'^996\d{9}$'
    value = value.raw_input
    if not re.match(pattern, value):
        raise serializers.ValidationError("Номер телефона должен быть в формате 996XXXXXXXXX.")
