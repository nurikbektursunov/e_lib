import os

import pyotp
import requests
from pyotp import TOTP

from config.settings import OTP_INTERVAL_SECONDS, MEGADRIVE_API, EVENT_ID, PROJECT_ID


def get_totp() -> TOTP:
    return pyotp.TOTP('base32secret3232', interval=OTP_INTERVAL_SECONDS)


def generate_otp(user):
    totp = get_totp()
    otp_instance = totp.now()
    user.otp_code = otp_instance
    user.otp_is_used = False
    user.save()
    return otp_instance


def send_otp_via_sms(phone_number, otp_code):
    query_params = {
        "eventId": EVENT_ID,
        "projectId": PROJECT_ID,
        "msisdn": phone_number,
        "code": otp_code,
    }
    try:
        # os.environ.pop('http_proxy', None)
        # os.environ.pop('https_proxy', None)
        # response = requests.post(MEGADRIVE_API, params=query_params, timeout=5)
        # response.raise_for_status()

        return {'OTP': 'Код подтверждения отправлен'}
    except requests.exceptions.HTTPError as http_err:
        raise requests.exceptions.HTTPError(http_err)
    except Exception as err:
        print(f"Other error occurred: {err}")
        raise err
