from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers

from users.models import User
from users.validators import validate_phone_number


class PhoneNumberSerializer(serializers.Serializer):
    phone_number = PhoneNumberField(validators=[validate_phone_number])

    def validate(self, data):
        phone_number = data.get("phone_number")
        data["phone_number"] = phone_number.raw_input
        return data


class UserSerializer(PhoneNumberSerializer, serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "phone_number",
            "full_name",
            "age",
            "user_type",
            "edu_institution",
        )

    def validate_phone_number(self, phone_number):
        if User.objects.filter(phone_number=phone_number).exists():
            raise serializers.ValidationError(
                "Пользователь с таким номером уже существует."
            )
        return phone_number


class OTPSerializer(PhoneNumberSerializer):
    otp_code = serializers.CharField(max_length=6)


class UserTokenSerializer(serializers.Serializer):
    refresh = serializers.CharField(max_length=255)
    access = serializers.CharField(max_length=255)
