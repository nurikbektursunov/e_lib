from rest_framework.renderers import JSONRenderer

from config.exceptions import ServiceUnavailable


class APIRenderer(JSONRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        response = renderer_context['response']

        if str(response.status_code).startswith('2'):
            response_data = {
                "status": "SUCCESS",
                "data": data,
                "message": None
            }
        else:
            try:
                error_massages = [data[key][0] for key in data]
                message = ', '.join(error_massages)
            except Exception:
                message = ServiceUnavailable.default_detail
            print()
            response_data = {
                "status": "ERROR",
                "data": None,
                "message": message
            }

        return super().render(response_data, accepted_media_type, renderer_context)
